import os
import json
from turtle import distance
#import datetime
from sqlalchemy import create_engine
from haversine import haversine

db_name = 'database'
db_user = 'username'
db_pass = 'secret'
db_host = 'db'
db_port = '5432'

# Connecto to the database
db_string = 'postgresql+psycopg2://{}:{}@{}:{}/{}'.format(db_user, db_pass, db_host, db_port, db_name)
db = create_engine(db_string)

def load_db():
    # Open JSON file
    f = open('starlink_historical_data.json')
    # Return Dictionary from File
    jdata = json.load(f)
    j = 0
    for i in jdata:
        j += 1
        jd = []
        jd.append(str(i['spaceTrack']['CREATION_DATE']))
        jd.append(str(i['id']))
        jd.append('0' if (str(i['latitude'])=='None') else str(i['latitude']))
        jd.append('0' if (str(i['longitude'])=='None') else str(i['longitude']))
        result = add_new_row(jd)
        if j % 100 == 0:
            print(j,' records loaded...')
    x = 'Done Loading DB.\nTotal records loaded: ' + str(j)
    return x

#def get_last_pos(y):
#    return y

#def get_closest(z):
#    return z

def add_new_row(n):
    # Insert a new number into the 'spacetrack' table.
    #date_time_obj = datetime.datetime.strptime(n[0], '%Y-%m-%dT%H:%M:%S')
    #n[0]=datetime.datetime.timestamp(date_time_obj)
    tDate = n[0].split('T')
    n[0] = str(tDate[0] + ' ' + tDate[1])
    sqlcom = "INSERT INTO space_track (creation_date,satid,lat,lon) " + "VALUES (TO_TIMESTAMP('" + str(n[0]) + "','YYYY-MM-DD HH24:MI:SS'),'" + str(n[1]) + "'," + str(n[2]) + "," + str(n[3]) + ");"

    return db.execute(sqlcom)

def get_rows_by_satid(id):
    # Retrieve the last record inserted in 'space_track' with satid = '5f36cb59bd8883000627408d'
    query = "" + \
            "SELECT * " + \
            "FROM space_track " + \
            "WHERE satid = '" + str(id) + "' " + \
            "ORDER BY creation_date DESC;" 

    result_set = db.execute(query)  
    #for (r) in result_set:  
    #    return r[0]
    return result_set

def get_rows_by_distance(lat,lon):
    # Retrieve the last record inserted in 'space_track' with satid = '5f36cb59bd8883000627408d'
    query = "" + \
            "SELECT * " + \
            "FROM space_track " + \
            "WHERE lat <> 0 " + \
            "AND lon <> 0;"

    result_set = db.execute(query)
    destination = (float(lat), float(lon))
    max_distance = 30000
    idx = 0
    for r in result_set:
        idx += 1
        origin = (float(r[2]),float(r[3]))
        dist = haversine(origin, destination)
        if dist < max_distance:
            max_distance = dist
            result = r
        if idx % 100 == 0:
            strtemp = 'Processed ' + str(idx) + ' records...'
            print(strtemp)

    #    return r[0]
    return result, max_distance

def mainMenu():
    input("...Press Enter to Continue...")
    os.system('cls||clear')
    print('MAIN MENU\n\n\n')
    print("Please select from the following:\n")
    print("1- Query Last Known Position of Satellite by id\n")
    print("2- Query When and Which SatId was Closest to a given Position\n")
    print('3- Quit Application\n')
    print("\n")
    usrSel = input("Enter your choice: ")
    
    if usrSel == '1':
        result = get_rows_by_satid('5f36cb59bd8883000627408d')
        for r in result:
            print(r[0],r[1],r[2],r[3])
            break
        #print(result[0],result[1],result[2],result[3])
    elif usrSel == '2':
        print('\n')
        lat = input("Enter Latitude: ")
        lon = input("Enter Longitude: ")
        result, dist = get_rows_by_distance(lat,lon)
        print('\n')
        print(result[0],result[1],result[2],result[3])
        print('Distance: ', dist)
    elif usrSel == '3':
        return 'q'
    else:
        print('Invalid Option\n')
    return 'c'

if __name__ == '__main__':
    print(load_db())
    result = 'c'
    while result == "c":
        result = mainMenu()
