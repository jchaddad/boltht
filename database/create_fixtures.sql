/*
  Created Table to store values from Starlink Historical Data
  I decided to go with text for latitude and longitude as value would lose accuracy when rounded or converted
  the ID used for satid is the root id field of each record
*/
CREATE TABLE IF NOT EXISTS space_track (
  creation_date TIMESTAMP,
  satid TEXT,
  lat DOUBLE PRECISION,
  lon DOUBLE PRECISION
  );
