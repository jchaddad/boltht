# Tutorial_DockerPythonPostgres

This projects was written using the Tutorial_DockerPythonPostgres structure included in the zip file

## Create Containers

    Commands to create the Containers:
    > docker compose build
    > docker compose up -d

    On Database creation, create_fixtures.sql is executed to create Starlink data Table space_track

## Run Application

    Command to run the application:
    > docker compose exec app python app.py

    On Startup, Data is automatically loaded to database through load_db() Function.

## Get Last known position by Satellite ID(id)
    query = "" + \
            "SELECT * " + \
            "FROM space_track " + \
            "WHERE satid = '" + str(id) + "' " + \
            "ORDER BY creation_date DESC;" 

    Output is first record of returned recordset

## Get Closest Satelite to coordinates throughout data history
    Haversine function used to get distance for all records
    Min distance returned through comparison of distances
